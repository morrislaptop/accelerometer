var gulp = require('gulp');
var wiredep = require('wiredep').stream;

gulp.task('bower', function () {
  gulp.src('./{craig,graphs}.html')
    .pipe(wiredep())
    .pipe(gulp.dest('.'));
});